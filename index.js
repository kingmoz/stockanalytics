var express = require('express');
var http = require('http');
var dateformat = require('dateformat');
var Moment = require('moment-timezone');
var emailService = require('./js/emailService');
var async = require('async');
var _ = require('lodash');
var fs = require('fs');
var holidayList = JSON.parse(fs.readFileSync("./data/holiday_hk.json")).dates;

var app = express();

var DAY = 24 * 60 * 60 * 1000;
var minimunDayOfData = 500;
var stockList = ["'^HSI'", "'0316.HK'", "'3988.HK'", "'0388.HK'"];

app.get('/', function (req, res) {
    res.send("Hello World");
});

app.get('/health', function (req, res) {
    res.send("200");
});

app.listen(process.env.PORT || 3000, function () {
    console.log('Server start! Port: ' + (process.env.PORT || 3000));
    sendRegularEmail();
});

function sendRegularEmail () {
    var tmrTS = (parseInt(new Date().getTime() / DAY) + 1) * DAY;
    var timeDifferent = tmrTS - new Date().getTime();
    console.log("[sendRegularEmail] timeDifferent: " + timeDifferent);
    setTimeout(function() {
        processBuySellSignals();
        sendRegularEmail();
    }, timeDifferent);
}

function processBuySellSignals() {
    var endTS = new Date(),
        startTS = endTS - 24 * 60 * 60 * 1000 * minimunDayOfData;
    var startDate = Moment.tz(startTS, 'Asia/Hong_Kong').format("YYYY-MM-DD"),
        endDate = Moment.tz(endTS, 'Asia/Hong_Kong').format("YYYY-MM-DD");

    var parallelList = [];
    for (var i in stockList) {
        console.log();
        parallelList.push(function(i) {
            return function(callback) {
                processBuySellSignal(startDate, endDate, stockList[i], callback);
            }
        }(i));
    }

    async.parallel(parallelList, function(err, results) {
        if (err) {
            console.log("[processBuySellSignals] Error in async.parallel: " + err);
        } else {
            var mailBody = emailService.generateMailBody(results);
            console.log("[processBuySellSignals] mailBody: " + mailBody);
            emailService.sendMail(mailBody);
        }
    });

}

function processBuySellSignal(startDate, endDate, stockName, callback) {
    var uri = '/v1/public/yql',
    yql = '?q=select Close, Date from yahoo.finance.historicaldata where symbol in ('
        + stockName + ') and startDate = "' + startDate
        + '" and endDate = "' + endDate + '"&format=json&env=store://datatables.org/alltableswithkeys&jsonCompat=new';

    console.log("[" + stockName + "] yql: " + yql);

    http.get({
        host: 'query.yahooapis.com',
        path: encodeURI(uri + yql),
    }, function(response) {
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            // Data reception is done, do whatever with it!
            var data = JSON.parse(body);
            data = data.query && data.query.results ? data.query.results.quote : undefined;
            if (!data)
                callback("[" + stockName + "] [processBuySellSignal] Error! Cannot parse YQL result", smaIntercept);

            var smaIntercept = processSMAIntercept(stockName, data, 50, 250);
            callback(null, smaIntercept);
        });
    });
}

/*
 *  processSMAIntercept process SMA intercept of firstSMA and secondSMA
    stockName Stock name (for logging)
    data Object List of stock price with lenght >= day of longSMA
    shortSMA Integer Short-term averages
    longSMA Integer long-term averages
    return Object
        - signal signal for buy(1) or sell(-1) or do nothing (0)
        - curlongSMA sma for long term average
        - curshortSMA sma for short term average
*/
function processSMAIntercept(stockName, data, shortSMA, longSMA) {
    if (!data) return -1;

    var curshortSMA = 0, ytdshortSMA = 0, curlongSMA = 0, ytdlongSMA = 0;
    var holidayCount = 0;
    var holidayIndex = 0;

    // Loop longSMA + 1 times for ytdlongSMA
    for (var i=0; i<longSMA + 1 + holidayCount; i++) {
        if (data[i].Date === holidayList[holidayIndex]) {
            console.log("[" + stockName + "] Date Doubled: " + data[i].Date);
            holidayCount++;
            holidayIndex++;
            continue;
        } else if (data[i].Date < holidayList[holidayIndex]) {
            holidayIndex ++;
        }
        var close = parseFloat(data[i].Close);

        if (i < longSMA + holidayCount)
            curlongSMA += close;
        if (i < shortSMA + holidayCount)
            curshortSMA += close;
        if (i > 0 && i < shortSMA + 1 + holidayCount)
            ytdshortSMA += close;
        if (i > 0 && i < longSMA + 1 + holidayCount)
            ytdlongSMA += close;
    }
    console.log("[" + stockName + "] index " + i + " End Date for longSMA: " + data[i].Date);

    curlongSMA /= longSMA;
    curshortSMA /= shortSMA;
    ytdlongSMA /= longSMA;
    ytdshortSMA /= shortSMA;

    var signal = 0;
    if (curshortSMA >= curlongSMA && ytdshortSMA <= ytdlongSMA) {
        signal = 1;
    } else if (curshortSMA <= curlongSMA && ytdshortSMA >= ytdlongSMA) {
        signal = -1;
    }
    console.log("[" + stockName + "] Signal: " + signal + "\ncurlongSMA: " + curlongSMA + "\ncurshortSMA: " + curshortSMA + "\nytdlongSMA: " + ytdlongSMA + "\nytdshortSMA: " + ytdshortSMA);

    return {
        stockName: stockName,
        signal: signal,
        closingPrice: data[0].Close,
        curshortSMA: Math.round(curshortSMA*1000)/1000,  //Convert to 3 decimal places
        curlongSMA: Math.round(curlongSMA*1000)/1000,
        diff: Math.round ((curshortSMA - curlongSMA)*1000)/1000
    };
}

function processRSI() {

}
