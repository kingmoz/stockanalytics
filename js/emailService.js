var api_key = 'key-d761828f30401ffba43eca883c765873';
var domain = 'sandboxf45201ad13374ba2b21e38f14d3850f5.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
var fs = require('fs');

/*
 * sendMail
 * body String content that will send to receiver
 */
function sendMail(mailBody) {
    var content = {
        from: 'King<stockanalyicservice@gmail.com>',
        to: 'kingsangchan@gmail.com',
        subject: "It's 8 o'clock!!!!",
        html: mailBody
    };
    mailgun.messages().send(content, function (error, body) {
        console.log(body);
    });
}

function generateMailBody(data) {
    var  mailBody =
        "<table><tr><th></th><th>Closing Price</th><th>SMA(250)</th><th>SMA(50)</th><th>Diff</th><th>Signal</th></tr>";
    for (var i in data) {
        console.log(data[i]);
        mailBody +=
            "<tr><td><h3><b>" + data[i].stockName + "</b></h3></td></tr><tr>"
            + "<td></td>"
            + "<td style='padding: 0 30px;text-align: center'>" + data[i].closingPrice + "</td>"
            + "<td style='padding: 0 30px;text-align: center'>" + data[i].curlongSMA + "</td>"
            + "<td style='padding: 0 30px;text-align: center'>" + data[i].curshortSMA + "</td>"
            + "<td style='padding: 0 30px;text-align: center'>" + data[i].diff + "</td>"
            + "<td style='padding: 0 30px;text-align: center'>" + data[i].signal + "</td></tr>";
    }
    mailBody += "</table>";
    return mailBody;
}

module.exports = {
    sendMail,
    generateMailBody
};
